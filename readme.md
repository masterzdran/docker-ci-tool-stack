# DOCKER CI-Stack [Beta]

## System Requirements
#### Docker Engine version: 1.13.0
#### Docker Compose version: 1.10.0
#### Docker Compose File format: 3
#### Docker Machine at least 6GB RAM and 2 CPUs


## Stack
#### jenkins:2.19.4
#### sonatype/nexus3:3.2.1
#### gitlab/gitlab-ce:8.16.5-ce.1
#### sonarqube:6.2
#### postgres:9.6.2

## Volumes Containers
#### jenkins-volume
#### nexus-volume
#### gitlab-ce-volume
#### sonarqube-volume
#### sonardb-volume


## Run
docker-compose up -d


## Endpoints
#### Jenkins
http://localhost:8081
credentials: no login required

#### Nexus
http://localhost:8090
credentials: admin/admin123

#### Gitlab
http://localhost:8080
credentials: root/5iveL!fe

#### SonarQube
http://localhost:9000
credentials: admin/admin


## Backup Volume Container
docker run --rm --volumes-from [Volume Name] -v $(pwd):/backup ubuntu tar cvf /backup/[Volume Name].tar /

## Restore Volume Container
$ docker run --rm --volumes-from [Volume Name] -v $(pwd):/backup ubuntu bash -c "cd / && tar xvf /backup/[Volume Name].tar --strip 1"
#### Special Note
If you create the Volume Containers first, you need to update the docker-compose.yml and uncomment the external attribute.


## Special Thanks:
[Marcel Birkner](https://blog.codecentric.de/en/2015/10/continuous-integration-platform-using-docker-container-jenkins-sonarqube-nexus-gitlab)
For the excelent article that help as start point.
